package com.chemasmas.globant.practicas.catalogocompose

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ChainStyle
import androidx.constraintlayout.compose.ConstraintLayout

@Composable
@Preview(showBackground = true, showSystemUi = true)
fun ConstarintExample1(){
    ConstraintLayout(modifier = Modifier.fillMaxSize()) {

        val (boxRed,boxBlue,boxYellow,boxMagenta,boxGreen) = createRefs()
        Box( modifier = Modifier
            .size(125.dp)
            .background(Color.Red)
            .constrainAs(boxRed) {
                top.linkTo(parent.top)
                start.linkTo(parent.start)
                end.linkTo(parent.end)
                bottom.linkTo(parent.bottom)
            }){
        }

        Box( modifier = Modifier
            .size(125.dp)
            .background(Color.Blue)
            .constrainAs(boxBlue) {
                top.linkTo(boxRed.bottom)
                start.linkTo(boxRed.end)
            }){

        }

        Box( modifier = Modifier
            .size(125.dp)
            .background(Color.Yellow)
            .constrainAs(boxYellow) {
                bottom.linkTo(boxRed.top)
                end.linkTo(boxRed.start)
            }){

        }

        Box( modifier = Modifier
            .size(125.dp)
            .background(Color.Magenta)
            .constrainAs(boxMagenta) {
                bottom.linkTo(boxRed.top)
                start.linkTo(boxRed.end)
            }){

        }

        Box(modifier = Modifier
            .size(125.dp)
            .background(Color.Green)
            .constrainAs(boxGreen) {
                top.linkTo(boxRed.bottom)
                end.linkTo(boxRed.start)
            }){

        }
    }
}

@Composable
@Preview(showSystemUi = true, showBackground = true)
fun ConstraintExampleGuide(){
    ConstraintLayout(modifier = Modifier.fillMaxSize()) {
//        val starGuide = createGuidelineFromTop(16.dp)
        val topGuide = createGuidelineFromTop(0.1f) // Porcentajes
        val boxRed = createRef()

        val startGuide = createGuidelineFromStart(0.25f)

        Box(modifier = Modifier
            .size(125.dp)
            .background(Color.Red)
            .constrainAs(boxRed) {
                start.linkTo(startGuide)
                top.linkTo(topGuide)
            })
    }
}

@Composable
@Preview(showBackground = true, showSystemUi = true)
fun ConstraintExampleBarrier(){
    ConstraintLayout(modifier = Modifier.fillMaxSize()) {
        val (boxRed,boxGreen, boxYellow) = createRefs()
        val barrier = createEndBarrier(boxRed,boxGreen)

        Box(modifier = Modifier
            .size(125.dp)
            .background(Color.Red)
            .constrainAs(boxRed) {
                start.linkTo(parent.start, margin = 16.dp)

            })


        Box(modifier = Modifier
            .size(235.dp)
            .background(Color.Green)
            .constrainAs(boxGreen) {
                top.linkTo(boxRed.bottom)
                start.linkTo(parent.start, margin = 32.dp)
            })

        Box(modifier = Modifier
            .size(50.dp)
            .background(Color.Yellow)
            .constrainAs(boxYellow) {
                start.linkTo(barrier)
            })
    }
}

@Composable
@Preview(showSystemUi = true, showBackground = true)
fun ConstraintChainExample(){
    ConstraintLayout(modifier = Modifier.fillMaxSize()) {

        val (boxRed,boxGreen, boxYellow) = createRefs()

        Box(modifier = Modifier
            .size(75.dp)
            .background(Color.Red)
            .constrainAs(boxRed) {
                start.linkTo(parent.start)
                end.linkTo(boxGreen.start)
            })


        Box(modifier = Modifier
            .size(75.dp)
            .background(Color.Green)
            .constrainAs(boxGreen) {
                start.linkTo(boxRed.end)
                end.linkTo(boxYellow.start)
            })

        Box(modifier = Modifier
            .size(75.dp)
            .background(Color.Yellow)
            .constrainAs(boxYellow) {
                start.linkTo(boxGreen.end)
                end.linkTo(parent.end)
            })

        createHorizontalChain(
            boxRed,boxGreen,boxYellow,
//            chainStyle = ChainStyle.Packed // TOdos Juntos
//            chainStyle = ChainStyle.Spread // Los coloca con separacions igual de distanciadas, incluida su distancia a los bordes exteriores
            chainStyle = ChainStyle.SpreadInside // los espacia entre ellos, pero los pega a los bordes
        )

//        createVerticalChain()

    }
}