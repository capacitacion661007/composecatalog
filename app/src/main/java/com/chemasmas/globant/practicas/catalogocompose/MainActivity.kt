package com.chemasmas.globant.practicas.catalogocompose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.chemasmas.globant.practicas.catalogocompose.ui.theme.CatalogoComposeTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CatalogoComposeTheme {
                // A surface container using the 'background' color from the theme
                Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colors.background) {
                    MyRow()
                }
            }
        }
    }
}

//@Composable
//fun Greeting(name: String) {
//    Text(text = "Hello $name!")
//}
//
//@Preview(showBackground = true)
//@Composable
//fun DefaultPreview() {
//    CatalogoComposeTheme {
//        Greeting("Android")
//    }
//}

@Composable
fun MyRow(){
    Row(
        modifier = Modifier
            .fillMaxSize()
            .horizontalScroll(
                rememberScrollState()
            )
        ,
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Text("Ejemplo 1", modifier = Modifier
            .background(Color.Blue)
            .width(100.dp) )
        Text("Ejemplo 1", modifier = Modifier
            .background(Color.Blue)
            .width(100.dp) )
        Text("Ejemplo 1", modifier = Modifier
            .background(Color.Blue)
            .width(100.dp) )
        Text("Ejemplo 1", modifier = Modifier
            .background(Color.Blue)
            .width(100.dp) )
        Text("Ejemplo 1", modifier = Modifier
            .background(Color.Blue)
            .width(100.dp) )
    }
}


@Composable
fun MyColumn(){
    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(
                rememberScrollState()
            ),
        verticalArrangement = Arrangement.SpaceEvenly
    ) {
//Sin usar Space between
//        Text("Ejemplo 1", modifier = Modifier.background(Color.Blue).weight(1f))
//        Text("Ejemplo 2", modifier = Modifier.background(Color.Red).weight(1f))
//        Text("Ejemplo 3", modifier = Modifier.background(Color.Cyan).weight(1f))
//        Text("Ejemplo 4", modifier = Modifier.background(Color.Magenta).weight(1f))
        Text("Ejemplo 1", modifier = Modifier
            .background(Color.Blue)
            .fillMaxWidth()
            .height(100.dp))
        Text("Ejemplo 2", modifier = Modifier
            .background(Color.Red)
            .fillMaxWidth()
            .height(100.dp))
        Text("Ejemplo 3", modifier = Modifier
            .background(Color.Cyan)
            .fillMaxWidth()
            .height(100.dp))
        Text("Ejemplo 4", modifier = Modifier
            .background(Color.Magenta)
            .fillMaxWidth()
            .height(100.dp))
        Text("Ejemplo 4", modifier = Modifier
            .background(Color.Magenta)
            .fillMaxWidth()
            .height(100.dp))
        Text("Ejemplo 4", modifier = Modifier
            .background(Color.Magenta)
            .fillMaxWidth()
            .height(100.dp))
        Text("Ejemplo 4", modifier = Modifier
            .background(Color.Magenta)
            .fillMaxWidth()
            .height(100.dp))
        Text("Ejemplo 4", modifier = Modifier
            .background(Color.Magenta)
            .fillMaxWidth()
            .height(100.dp))
        Text("Ejemplo 4", modifier = Modifier
            .background(Color.Magenta)
            .fillMaxWidth()
            .height(100.dp))
        Text("Ejemplo 4", modifier = Modifier
            .background(Color.Magenta)
            .fillMaxWidth()
            .height(100.dp))
        Text("Ejemplo 4", modifier = Modifier
            .background(Color.Magenta)
            .fillMaxWidth()
            .height(100.dp))
        Text("Ejemplo 4", modifier = Modifier
            .background(Color.Magenta)
            .fillMaxWidth()
            .height(100.dp))
        Text("Ejemplo 4", modifier = Modifier
            .background(Color.Magenta)
            .fillMaxWidth()
            .height(100.dp))
        Text("Ejemplo 4", modifier = Modifier
            .background(Color.Magenta)
            .fillMaxWidth()
            .height(100.dp))
        Text("Ejemplo 4", modifier = Modifier
            .background(Color.Magenta)
            .fillMaxWidth()
            .height(100.dp))
        Text("Ejemplo 4", modifier = Modifier
            .background(Color.Magenta)
            .fillMaxWidth()
            .height(100.dp))

    }
}

@Composable
fun MyBox(){
    Box(modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center ) {
        Box( modifier = Modifier
            .width(200.dp)
            .height(200.dp)
            .background(Color.Cyan)
            .verticalScroll(
                rememberScrollState() //Scroll State
            ),
            contentAlignment = Alignment.Center
        ){
            Text("Ejemplo asadasdads")
        }
    }
}

@Composable
@Preview(showBackground = true)
fun PreviewMyBox(){
    CatalogoComposeTheme {
        MyBox()
    }
}

@Composable
@Preview(showBackground = true)
fun PreviewMyColumn(){
    CatalogoComposeTheme {
        MyColumn()
    }
}

@Composable
@Preview(showBackground = true)
fun PreviewMyRow(){
    CatalogoComposeTheme {
        MyRow()
    }
}

@Composable
fun MyComplexLayout(){
    Column(modifier = Modifier.fillMaxSize()) {
        Box( modifier = Modifier
            .weight(1f)
            .fillMaxSize()
            .background(Color.Cyan),
        contentAlignment = Alignment.Center) {
            Text("Ejemplo 1")
        }
        Spacer(modifier = Modifier
            .width(0.dp)
            .height(30.dp))
        Row( modifier = Modifier
            .fillMaxWidth()
            .weight(1f) ) {
            Box( modifier = Modifier
                .weight(1f)
                .background(Color.Red)
                .fillMaxSize() ,
                contentAlignment = Alignment.Center) {
                Text("Ejemplo 2")
            }

            Box( modifier = Modifier
                .weight(1f)
                .background(Color.Green)
                .fillMaxSize(),
                contentAlignment = Alignment.Center) {
                Text("Ejemplo 2")
            }
        }
        Box( modifier = Modifier
            .weight(1f)
            .fillMaxSize()
            .background(Color.Magenta),
        contentAlignment = Alignment.BottomCenter) {
                Text("Ejemplo 4")
        }
    }
}

@Composable
@Preview(showBackground = true)
fun PreviewMyComplexLayout(){
    CatalogoComposeTheme {
        MyComplexLayout()
    }
}

@Composable
@Preview(showBackground = true, showSystemUi = true)
fun MyStateExample(){

    //REvisar notas Readme sobre remember
//    val counter = remember { mutableStateOf(0) }
    //Revisar notas sobre rememberSaveable en readme
//    val counter = rememberSaveable {
//        mutableStateOf(0)
//    }

    //Revisar notas en el Readme
    var counter by rememberSaveable {
        mutableStateOf(0)
    }

//    val a = remember {
//        mutableStateOf(1)
//    }

    Column( modifier = Modifier.fillMaxSize(),
    verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
//        Button(onClick = { counter.value += 1 }) {
        Button(onClick = { counter += 1 }) {
            Text(text = "Pulsar!")
        }
//        Text(text = "He sido Pusado ${counter.value} veces")
        Text(text = "He sido Pusado $counter veces")
    }
}