# Dudas y respuestas

En este readme ire registrando las dudas que me surgen al ir desarollando el curso.

## remember

La funcion remember esta declarada de la siguiente manera

```kotlin
@Composable
inline fun <T> remember(crossinline calculation: @DisallowComposableCalls () -> T): T =
    currentComposer.cache(false, calculation)
```


De esa declaracion podemos obervar lo siguiente

+ Esta anotado como `@Composable`, lo que nos indica que puede ser usado como parte de una compocision. Solo puede ser llamada dentro de otra funcion anotada como `@Composable`. Estas funciones comparten un 'contexto composable' cuando una funcion  llama a otra [Android Doc](https://developer.android.com/reference/kotlin/androidx/compose/runtime/Composable#Composable())

+ La indicacion de `inline`, nos indica que tratara de copiar el cuerpo de la funcion, el modificador `crossinline` nos indica que la funcion enviada en la lambda (`calculation`) sera invocada dentro en el lugar que sera copiada.

+ `DisallowComposableCalls` La documentacion menciona algo de sobre evitar que la llamada de la funcion anotada con esta anotacion, se necesita ahondar en ese tema

+ `currentComposer` es el contexto composable que comparten 

## Saver y @Parcelize

La firma de `rememberSaveable` es algo mas compleja que la de remember

```kotlin
@Composable
fun <T : Any> rememberSaveable(
    vararg inputs: Any?,
    saver: Saver<T, out Any> = autoSaver(),
    key: String? = null,
    init: () -> T
): T {
```

En este caso, mientras que remmeber recordara el resultado de la funcion `calculation`, `rememberSaveable` recordara inicialmente el resultado de invocar `init()`, se comporta parecido a `remember` con la principal diferencia que sobrevivira a cambios de configuracion, como el cambio de orientacion.

+ `inputs` son los parametros a los que estaremos escuchando, si alguno de estos cambia, la funcion `init` sera reinvocada

+ `saver` una clase que define como se almacenara y recuperara la informacion.

+ `key` define una llave para almacenar el valor, si no se provee se calculara una por cada ejecucion nueva 

Para objetos complejos, se puede usar `@Parcelize` para guardar objetos complejos


## Temas de Composables

Si declaramos una funcion `@Composable` con un solo elemento de texto de la siguiente manera.

```kotlin
@Composable
fun Foo() {
    Text("foo")
}
```

## High Order Functions (HOF) [Funciones de orden superior]

Una de las caracteristicas de las HOF es que estas al ser procesadas, son envueltas en una clase que extiende de `FunctioN`

## Funciones inline

Cuando pasamos una funcion como parametro a otra funcion, el compilador genera una clase que estiende de `FunctioN`. Si se llaman muchas funciones de esta manera, esto puede incrementar el uso de memoria y tener un impacto en el rendimiento del programa.

La funcion que realiza es remplazar el codigo del cuerpo de la funcion, por su llamada, y el parametro con la lambda  tambien es aplanado.

SI la funcion marcada como inline, no tienen ningun parametro que sea una lambda,  nos marcara un mensaje diciendo que muy probablemente esa funcion no sea necesaria. Es poco recomendado usar inline en funciones grandes.

No podemos usar un return dentro de las lambdas, si queremos usar return requerimos el uso de etiquetas.

Se pierde acceso a cualquier variable privada o funcion dentro de la clase donde esta la funcion a aplicar el inline

## Clases Inline [Experimental]()

De este tema he encontrado algunos ejemplos y lo usan para reforzar el sistema de tipado.

El ejemplo que halle es el siguiente:

```kotlin
//Sin clases inline
data class Recipe(id:UUID)
data class Ingredient(id:UUID,recipe:UUID)

val recipeId = UUID.randomUUID()

val ingredienteIncorrecto = Ingredient(recipeId,recipeId)
```

Como se ve en el  ejemploexiste la posibilidad de errar el valor a enviar, una solucion propuesta es la siguiente:


```kotlin
inline class RecipeId(id: UUID)
inline class IngredientId(id:UUID)

data class Recipe(id:RecipeId)
data class Ingredient(id:IngredientId,recipe:RecipeId)

val ingredientId = IngredientId(UUID.randomUUID())

//Esta parte no compilara
val ingredienteIncorrecto = Ingredient(ingredientId,ingredientId)

val recipeId = RecipeId(UUID.randomUUID())

//Funciona correctamente y limita lo que podemos enviarle
val correcta = Ingredient(ingredientId,recipeId)
```


### No inline

Si se quiere limitar que alguno de los parametros con lambda, se puede usar el modificador noinline, y esto causa que ese elemento tenga el comportamiento usual , aunque la funcion este marcada como inline

### Crossinline

Este modificador se usa para markar un parametro en lambda que no debe permitir un return no local ( Investigar que es esto?). 

//TODO


## Operador by

Se puede entender como una forma corta de "proveido por (provide by)"  la firma del operador es la siguiente


```kotlin
@Suppress("NOTHING_TO_INLINE")
inline operator fun <T> State<T>.getValue(thisObj: Any?, property: KProperty<*>): T = value
```

```kotlin
@Suppress("NOTHING_TO_INLINE")
inline operator fun <T> MutableState<T>.setValue(thisObj: Any?, property: KProperty<*>, value: T) {
    this.value = value
}
```

Dependiendo de la declaracion, se requiere proveer uno o el otro. Si la variable es declarada como `val` solo nos provera del operador `getValue` , si es declarada como `var` nos proveera de `getValue` y `setValue`.

Este operador tiene como base el patron `delegate` que nos da una alternativa a la herencia, 

El siguiente bloque de codigo contiene una solucion implementando usando delegacion y mostrando como se puede intercambiar el proveedor de la caracteristica.

```kotlin
// Funcion a ser proveida
interface Base {
    fun print()
}

class BaseImpl(val x: Int) : Base {
    override fun print() { println(x) }
}

class BaseImpl2: Base {
    override fun print() { println("Este no recibe valor") }
}

class Derived(b: Base) : Base by b

class Derived2(b2: Base) :Base by b2

fun main() {
    val b = BaseImpl(10)
    Derived(b).print()
    
    Derived2(BaseImpl(5)).print()
    Derived2(BaseImpl2()).print()
}
```


```
```